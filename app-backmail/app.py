from flask import Flask, request, jsonify
from flask_mail import Mail
from flask_mail import Message
from flask_cors import cross_origin

app = Flask(__name__)

mail_settings = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    "MAIL_USERNAME": "alsjdkfmgn@gmail.com",
    "MAIL_PASSWORD": "v4~\"HT88"
}

app.config.update(mail_settings)
mail = Mail(app)

@app.route('/message', methods = ['POST'])
@cross_origin()
def send_mail():
    if request.method == 'POST':
        content = request.json
        msg = Message(subject='Test daw app',
                      sender=app.config.get("MAIL_USERNAME"),
                      recipients=[content['email']], #vine din body, unde este pusa din UI
                      body=content['message'])
        mail.send(msg)
        return jsonify('succes')
    else:
        return jsonify('method not allowed')

if __name__ == '__main__':
    app.run()
