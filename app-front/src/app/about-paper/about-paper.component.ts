import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../services/authentication.service';
import { Credentials } from '../models/credentials.model';
import { Comment } from '../models/comment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-about-paper',
  templateUrl: './about-paper.component.html',
  styleUrls: ['./about-paper.component.scss']
})
export class AboutPaperComponent implements OnInit {
  loggedIn: Credentials;
  comments$: Observable<Comment[]>;
  username: string;
  comment: Comment;
  dates : Date[];
  mapDates: Map<string, number>;
  private baseURL = "http://localhost:8000"; // url din php 

  view: any[] = [600, 400];

  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Days';
  showYAxisLabel = true;
  yAxisLabel = 'Number of comments';
  timeline = true;

  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };

  // data goes here
 single = [
  {
    "name": "Sunday",
    "value": 0
  },
  {
    "name": "Monday",
    "value": 0
  },
  {
    "name": "Tuesday",
    "value": 0
  },
  {
    "name": "Wednesday",
    "value": 0
  },
  {
    "name": "Thursday",
    "value": 0
  },
  {
    "name": "Friday",
    "value": 0
  },
  {
    "name": "Saturday",
    "value": 0
  }
];

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient, private auth: AuthenticationService, private router: Router) { 
    
  }

  ngOnInit() {
    if (!sessionStorage.getItem('username')) {
      this.router.navigateByUrl('login');
    }
    this.loggedIn = JSON.parse(sessionStorage.getItem('username')) as Credentials;
    this.comments$ = this.http.get<Comment[]>(this.baseURL + "/api/comments");
    this.comments$.subscribe(comm => {
      for (let c of comm) {
        this.dates = [];
        this.dates.push(new Date(c.created_at));
        this.createGraph();
      }
    });
    
  }

  createGraph() {
    let res = {};
    for (let dt of this.dates) {
      let day = dt.getDay();
      this.single[day].value = (this.single[day].value || 0) + 1;
    }
  }


  onSubmit(event, message: HTMLInputElement){
    event.preventDefault();
    this.comment = new Comment();
    this.comment.username = String(this.loggedIn);
    this.comment.message = message.value;
    this.http.post<Comment>(this.baseURL + "/api/postComment", JSON.stringify(this.comment), this.httpOptions).toPromise()
    .then(() => alert('success'))
    .catch(() => alert('failure'));
  }


}
