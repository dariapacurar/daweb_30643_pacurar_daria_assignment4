import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {AgmMap, MouseEvent, MapsAPILoader  } from '@agm/core';  
import {GoogleMapsAPIWrapper} from '@agm/core';
import {ILatLng} from '../directions-map.directive';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})

export class ContactComponent implements OnInit {
  personalName: string = "Pacurar Daria";
  personalEmail: string = "pacudari24@gmail.com";
  personalAddress: string = "Piata Unirii, nr. 1, Cluj Napoca, jud. Cluj, Romania";

  ///////// Maps
  // home adress
  origin: ILatLng = {
    latitude: 46.7700702,
    longitude: 23.5881637
  };

  destination: ILatLng = {
    latitude: 45.7809485,
    longitude: 23.6030027
  };

  displayDirections = true;
  zoom = 14;

  getAddress: [number, number];

  constructor(private _snackBar: MatSnackBar, private translate: TranslateService, 
    private httpClient: HttpClient, private router: Router, private apiloader: MapsAPILoader,
    private gmapsApi: GoogleMapsAPIWrapper) { 
      
    if (!sessionStorage.getItem('username')) {
      this.router.navigateByUrl('login');
    }
  }

  ngOnInit() {
    this.get();
  }

  sendMail (mail: string, text: string) {
    this.httpClient.post<string>('http://127.0.0.1:5000/message', {"email": mail,"message": text})
    .toPromise().then(() => alert('Successfully sent mail')).catch(() => alert('Failed to send mail'))
  }

get() {  
    if (navigator.geolocation) {  
        navigator.geolocation.getCurrentPosition((position: Position) => {  
            if (position) { 
              this.destination = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
              }; 
                this.getAddress = [position.coords.latitude, position.coords.longitude];  
                this.apiloader.load().then(() => {  
                    let geocoder = new google.maps.Geocoder;  
                    let latlng = {  
                        lat: this.destination.latitude,  
                        lng: this.destination.longitude  
                    };  
                    geocoder.geocode({  
                        'location': latlng  
                    }, function(results) {  
                        if (results[0]) {  
                            this.currentLocation = results[0].formatted_address;  
                            console.log("Locatia curenta: " + this.currentLocation);  
                        } else {  
                            console.log('Not found');  
                        }  
                    });  
                });  
        }  
      });  
    }
}  

}