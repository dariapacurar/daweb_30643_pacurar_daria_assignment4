import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Credentials } from '../models/credentials.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  personalName: string = "Pacurar Daria";
  loggedIn: Credentials;
  credential: Credentials;
  cc: Credentials;
  // credential: Observable<Credentials>;

  private baseURL = "http://localhost:8000"; // url din php 

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private router: Router, private http: HttpClient) {
    if (!sessionStorage.getItem('username')) {
      this.router.navigateByUrl('login');
    }
   }

  ngOnInit() {
    this.loggedIn = JSON.parse(sessionStorage.getItem('username')) as Credentials;
    this.cc = new Credentials();
    this.cc.username = String(this.loggedIn);
    this.http.post<Credentials>(this.baseURL + "/api/getUser", this.cc, this.httpOptions)
    .subscribe(profileData => {this.credential = profileData; console.log(this.credential.username);});
  }

  goToContact() {
    this.router.navigateByUrl('contact');
  }

  goToEdit() {
    this.router.navigateByUrl('edit');
  }
}
